import React, {Component} from 'react';
import './App.css';
import get from 'axios';
import WeatherList from './components/WeatherList';
import ZipForm from './components/ZipForm';
import CurrentDay from './components/CurrentDay';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      zipcode: "",
      city: {},
      dates: [],
      showWeatherList: false
    };

    this.url = "http://api.openweathermap.org/data/2.5/forecast/daily?zip=";
    this.apikey = "&units=imperial&appid=c59493e7a8643f49446baf0d5ed9d646";

    this.onFormSubmit = this.onFormSubmit.bind(this)
  }

onFormSubmit(zipcode){
  this.setState({showWeatherList: true})
  get(this.url + zipcode + this.apikey)
  .then(({data}) => {
    const{city, list: dates} = data;
    this.setState({zipcode, city, dates, selectedDate:null});
  })
  .catch(error => {
    alert(error);
  });
}

render() {
  return (
    <div className="App">
    <div class="heading">
    <h1>React Weather List</h1>
    </div>
      <ZipForm onSubmit={this.onFormSubmit} />
      <WeatherList days={this.state.dates}  showWeatherList={this.state.showWeatherList}/>
    </div>
  );
}
}

export default App;
