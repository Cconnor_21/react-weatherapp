import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button } from 'reactstrap';

class WeatherListItem extends Component{


  render() {
    const {day} = this.props;
    const date = new Date(day.dt * 1000);
    const imageUrl = "http://openweathermap.org/img/w/";
    const icon = day.weather[0].icon;
    const imageExt = ".png";
    const month = date.getMonth();
    const currentDay = date.getDate();
    return(
    <div class="card bg" onClick={() => this.props.handleClick(day, date, month, currentDay, imageUrl, icon, imageExt)}>
      <div class="card-body text-center">
        <img src={imageUrl + icon + imageExt}  alt="dailyweather" />
        <h3 class="card-text">{date.getMonth() + 1} / {date.getDate()}</h3>
        <p>{day.temp.min}F&deg; | {day.temp.max}F&deg;</p>
      </div>
    </div>
    );
  }
}

export default WeatherListItem;
