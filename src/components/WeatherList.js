import React, {Component} from 'react';
import WeatherListItem from './WeatherListItem';
import CurrentDay from './CurrentDay';

class WeatherList extends Component {
  constructor(props){
    super(props);
    this.state = {
      showCurrentDay: false,
      date: '',
      mintemp: '',
      maxtemp: '',
      month: '',
      currentDay: '',
      imageUrl: '',
      icon: '',
      imageExt: '',
      day: ''
    }
  }
  handleClick = (day, date , month, currentDay, imageUrl, icon, imageExt) => {
    this.setState({mintemp: day.temp.min})
    this.setState({maxtemp: day.temp.max})
    this.setState({date: date})
    this.setState({month: month})
    this.setState({currentDay: currentDay})
    this.setState({imageUrl: imageUrl})
    this.setState({icon: icon})
    this.setState({imageExt: imageExt})
    this.setState({showCurrentDay: true})
    this.setState({day: day})
  }
  render() {
    const {days} = this.props;
    if(this.props.showWeatherList == false)
    {
        return(
          <div>
          </div>
        );
    }
    else{
      return(
        <React.Fragment>
        <h4 className="header">Click a day to get more weather info</h4>
        <div class="card-group">
          {days.map((day, index) =>
          <WeatherListItem handleClick={this.handleClick} className="list-item" key={day.dt} day={day} index={index} />)}
        </div>
        <CurrentDay
          mintemp={this.state.mintemp}
          maxtemp={this.state.maxtemp}
          date={this.state.date}
          month={this.state.month}
          currentDay={this.state.currentDay}
          imageUrl={this.state.imageUrl}
          icon={this.state.icon}
          imageExt={this.state.imageExt}
          day={this.state.day}
          />
        </React.Fragment>
      );
    }
  }
}

export default WeatherList;
