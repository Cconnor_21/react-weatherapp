import React, {Component} from 'react';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button } from 'reactstrap';

class CurrentDay extends Component {
  render(){
    if(this.props.currentDay == false)
    {
        return(
          <div>
          </div>
        );
    }
    else{
      return(
        <div class="flex-container">
        <div class="card bg currentcard">
          <div class="card-body text-center">
            <h3>Daily Weather Info</h3>
            <img src={this.props.imageUrl + this.props.icon + this.props.imageExt}  alt="dailyweather" />
            <h3 class="card-text">{this.props.month} / {this.props.currentDay}</h3>
            <p>Max Temp: {this.props.mintemp}F&deg;</p>
            <p>Min Temp: {this.props.mintemp}F&deg;</p>
            <p>humidity: {this.props.day.humidity}F&deg;</p>
          </div>
        </div>
        </div>
      );
    }
  }
}

export default CurrentDay;
