import React, {Component} from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

class ZipForm extends Component{
  constructor(props){
    super(props);
    this.state = {
      zipcode: ""
    }
    this.inputUpdated = this.inputUpdated.bind(this);
    this.submitZipCode = this.submitZipCode.bind(this);
  }

  inputUpdated(e){
      const zipcode = e.target.value;
      //set the state of zip code to the entered zip code
      this.setState({zipcode: zipcode});
  }

  submitZipCode(e){
    //prevent default form submit
    e.preventDefault();
    const{zipcode} = this.state;
    const{onSubmit} = this.props;
    onSubmit(zipcode);
    this.setState({zipcode: ""});
  }

  render(){
    return(
        <Form onSubmit={this.submitZipCode}>
            <FormGroup>
              <Label for="exampleEmail" hidden>Email</Label>
              <div class="flex-container">
                <div class="mainInput">
                <Input class="mainInput" onInput={this.inputUpdated} value={this.state.zipcode}  name="zipcode" id="exampleEmail" placeholder="Enter your zip code" />
                </div>
              </div>
            </FormGroup>
          <Button color="info">submit</Button>{' '}
        </Form>
    );
  }
}

export default ZipForm;
